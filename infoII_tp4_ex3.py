class File:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        self.items.insert(0, item)

    def dequeue(self):
        return self.items.pop()

    def is_empty(self):
        return (self.items == [])

    def size(self):
        return len(self.items)

class File_attente_prioritaire(File):
    def __init__(self):
        super().__init__()

    def enqueue(self, item):
        self.items.insert(item[0], item[1])
